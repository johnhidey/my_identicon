import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :my_identicon, MyIdenticonWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "QVvTVQuP2zAUtb/LypJA+DMsHR79QSfe699Va8krvw/FKC7jEKe2MNv+o8n/xJ8b",
  server: false

# In test we don't send emails.
config :my_identicon, MyIdenticon.Mailer,
  adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
