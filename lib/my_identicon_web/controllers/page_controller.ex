defmodule MyIdenticonWeb.PageController do
  use MyIdenticonWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
