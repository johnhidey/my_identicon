// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
import "../css/app.css"
import "../vendor/materialize-css/js/materialize.min"

document.addEventListener('DOMContentLoaded', function() {
  var sidenavOptions = {};
  var elems = document.querySelectorAll('.sidenav');
  var instances = M.Sidenav.init(elems, sidenavOptions);
});
